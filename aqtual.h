/**********************************************************************
        Copyright 2014 Shannon Mackey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**********************************************************************/

#ifndef QOMPARATOR_H
#define QOMPARATOR_H

#include <QMainWindow>
#include <QActionGroup>
#include <QSettings>
#include <QMdiSubWindow>
#include <QSqlDatabase>
#include <QSortFilterProxyModel>

#include "modelwindow.h"
#include "about.h"
#include "usersettings.h"
#include "localdatamanager.h"
#include "results.h"

namespace Ui {
    class aQtual;
}

class aQtual : public QMainWindow
{
    Q_OBJECT

public:
    explicit aQtual(QWidget *parent = 0);
    ~aQtual();
    void closeEvent(QCloseEvent *event);

public slots:
    void prepareWindowMenu();

signals:

private:
    Ui::aQtual *ui;

    LocalDataManager *DataMan;

    QSqlDatabase LibraryDB;

    UserSettings *UserSet;
    QActionGroup *ProjectGroup;
    QActionGroup *DirtyGroup;
    int m_sequence;
    QList<QAction*> WindowActions;

    ModelWindow *createMdiChild();
    ModelWindow *createMdiChild(QString fname);
    ModelWindow *activeMdiChild();
    QMdiSubWindow *findMdiChild(const QString &filename);
    QColor sequenceColor(int);

    void setActiveSubWindow(QWidget *window);
    void openExistingFile(const QString &existingname);
    void prepareActions();
    void prepareWindowActions();
    void prepareGlobalDBs();
    void readSettings();
    void saveSettings();

private slots:
    void on_action_Situation_toggled(bool );
    void on_action_Results_toggled(bool );
    void on_actionClose_All_triggered();
    void on_action_Duplicate_triggered();
    void on_action_Save_triggered();
    void on_actionSave_All_triggered();
    void on_action_Close_triggered();
    void on_action_Open_triggered();
    void on_actionAbout_Qt_triggered();
    void on_action_About_aQtual_triggered();
    void on_action_New_triggered();
    void setDirty(bool);

    void on_action_Quit_triggered();
    void on_action_User_Settings_triggered();
    void on_action_Data_Manager_triggered();
};

#endif // QOMPARATOR_H




