/**********************************************************************
        Copyright 2014 Shannon Mackey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**********************************************************************/

#include <QApplication>
#include "aqtual.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    aQtual w;
    w.show();

    return a.exec();
}


// impromtu task management - to appear in QtCreator's To-Do Entries results pane
//TODO: sql model load from brand file
//TODO: thread per model
//TODO: undo/redo stack
//TODO: scripting interface
//TODO: model plugin interface - get/set, menu/toolbar actions

//TODO: network model engine
//TODO: engine plugin interface - get/set, menu/toolbar actions
//TODO: results plugin interface - get/set, menu/toolbar actions
//TODO: web service plugin interface - get/set, menu/toolbar actions
