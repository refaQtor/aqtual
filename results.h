/**********************************************************************
        Copyright 2014 Shannon Mackey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**********************************************************************/

#ifndef RESULTS_H
#define RESULTS_H

#include <QFrame>

namespace Ui {
class Results;
}

class Results : public QFrame
{
    Q_OBJECT
    
public:
    explicit Results(QWidget *parent = 0);
    ~Results();
    
protected:
    void changeEvent(QEvent *e);
    
private slots:
    void on_PeriodUnit_currentIndexChanged(int index);

private:
    Ui::Results *ui;
};

#endif // RESULTS_H
