/**********************************************************************
        Copyright 2014 Shannon Mackey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**********************************************************************/

#ifndef LOCALDATAMANAGER_H
#define LOCALDATAMANAGER_H

#include <QDialog>
#include <QSqlDatabase>
#include <QSqlRecord>
#include <QSqlTableModel>


namespace Ui {
    class LocalDataManager;
}

class LocalDataManager : public QDialog
{
    Q_OBJECT

public:
    explicit LocalDataManager(QWidget *parent = 0);
    ~LocalDataManager();

    void show();

    QSqlRecord selectWeatherLocation();

public slots:
    void prepareWeatherViews();

private slots:

    void on_WeatherView_doubleClicked(const QModelIndex &index);

    void on_WeatherCityFilter_textEdited(const QString &arg1);

    void on_WeatherCountryFilter_currentIndexChanged(const QString &arg1);

    void on_WeatherRegionFilter_currentIndexChanged(const QString &arg1);

    void on_WeatherFilterClear_clicked();

    void createNewLibraryDB();
private:
    Ui::LocalDataManager *ui;

    QSqlDatabase WeatherDB;
    QSqlDatabase LibraryDB;
    QSqlTableModel *WeatherLocTblMdl;

    bool prepareLocalDB();
    void prepareWeatherModels();
    void prepareWeatherFilters();
    void resetWeatherFilters();
    void resetCityFilter();
    void resetRegionFilter();
    void resetCountryFilter();
    void reloadModelViews();
//    void addPointsToMap();
};



#endif // LOCALDATAMANAGER_H
