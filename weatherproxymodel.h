/**********************************************************************
        Copyright 2014 Shannon Mackey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**********************************************************************/

#ifndef WEATHERPROXYMODEL_H
#define WEATHERPROXYMODEL_H

#include <QSortFilterProxyModel>

class WeatherProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    explicit WeatherProxyModel(QObject *parent = 0);
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const;
    void setCityFilterString(QString city) {CityFilterString = city;}
    void setRegionFilterString(QString region) {RegionFilterString = region;}
    void setCountryFilterString(QString country) {CountryFilterString = country;}

signals:

public slots:

private:
    QString CityFilterString;
    QString RegionFilterString;
    QString CountryFilterString;

};

#endif // WEATHERPROXYMODEL_H
