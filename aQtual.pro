#-------------------------------------------------
#Copyright (c) 2014, Shannon Mackey
#All rights reserved.

#Redistribution and use in source and binary forms, with or without
#modification, are permitted provided that the following conditions are met:

#1. Redistributions of source code must retain the above copyright notice, this
#   list of conditions and the following disclaimer.
#2. Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.

#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
#ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
#WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
#DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
#ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
#(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
#LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
#ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
#(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
#SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#-------------------------------------------------

QT       += core widgets sql xml

TARGET = aQtual
TEMPLATE = app

SOURCES += main.cpp\
        aqtual.cpp \
    modelwindow.cpp \
    usersettings.cpp \
    about.cpp \
    localdatamanager.cpp \
    situation.cpp \
    weatherproxymodel.cpp \
    results.cpp \
    model.cpp

HEADERS  += aqtual.h \
    usersettings.h \
    about.h \
    modelwindow.h \
    localdatamanager.h \
    situation.h \
    weatherproxymodel.h \
    results.h \
    model.h

FORMS    += aqtual.ui \
    usersettings.ui \
    about.ui \
    modelwindow.ui \
    localdatamanager.ui \
    situation.ui \
    results.ui

OTHER_FILES += \
    resources/default/common/address-book.png \
    resources/default/common/window-new.png \
    resources/default/common/utilities-system-monitor.png \
    resources/default/common/system-file-manager.png \
    resources/default/common/preferences-system.png \
    resources/default/common/preferences-system-windows.png \
    resources/default/common/media-floppy.png \
    resources/default/common/edit-copy.png \
    resources/default/common/document-open.png \
    resources/default/common/document-new.png \
    resources/default/common/dialog-information.png \
    resources/default/common/application_tile_horizontal.png \
    resources/default/common/application_side_expand.png \
    resources/default/common/application_side_contract.png \
    resources/default/common/application_cascade.png \
    resources/default/common/spreadsheet.png \
    resources/default/common/applications-internet.png \
    resources/default/common/weather-few-clouds.png \
    resources/default/common/editcopy.png \
    resources/default/common/editcut.png \
    resources/default/common/editdelete.png \
    resources/default/common/editpaste.png \
    resources/default/common/redo.png \
    resources/default/common/undo.png \
    resources/default/custom/aqtual-icon.png \
    resources/default/custom/custom.sql \
    resources/default/custom/custom.qss \
    docs/Apache2.txt

RESOURCES += \
    base.qrc
