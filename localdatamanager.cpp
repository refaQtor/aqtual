/**********************************************************************
        Copyright 2014 Shannon Mackey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**********************************************************************/

#include "localdatamanager.h"
#include "ui_localdatamanager.h"

#include <QMessageBox>
#include <QDir>
#include <QFile>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QDomDocument>
#include <QDebug>

LocalDataManager::LocalDataManager(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LocalDataManager)
{
    ui->setupUi(this);

    if (!prepareLocalDB())
        QMessageBox::critical(0, "Local Database Initialization Failure", "Project files may function, but you will be unable to add or change components.",QMessageBox::Cancel);
}

LocalDataManager::~LocalDataManager()
{
    WeatherDB.close();
    delete ui;
}

void LocalDataManager::show()
{
    QWidget::show();
    ui->PageList->show();
}

bool LocalDataManager::prepareLocalDB()
{
    WeatherDB = QSqlDatabase::addDatabase("QSQLITE", "weather");
    WeatherDB.setDatabaseName(QDir::toNativeSeparators(QDir::homePath().append(QDir::separator()))+"LocalWeather.sqlt");

    QFile wlibfile(WeatherDB.databaseName());
    if (!wlibfile.exists())
    {
        QMessageBox::critical(0,"File Access Error","Weather DB file doesn't exist: " + wlibfile.fileName(), QMessageBox::Cancel);
        return false;
    }
    else
    {
        if (!WeatherDB.open())
        {
            QMessageBox::critical(0,"File Access Error","Can't open Weather DB file: " + wlibfile.fileName(), QMessageBox::Cancel);
            return false;
        }
        else
        {
            prepareWeatherModels();
        }
        return true;
    }
}

void LocalDataManager::prepareWeatherModels()
{
    WeatherLocTblMdl = new QSqlTableModel(this,WeatherDB);
    WeatherLocTblMdl->setTable("location");
    WeatherLocTblMdl->setEditStrategy(QSqlTableModel::OnManualSubmit);
    WeatherLocTblMdl->setSort(1,Qt::AscendingOrder);
    WeatherLocTblMdl->select();
    prepareWeatherViews();
}

void LocalDataManager::prepareWeatherViews()
{
    ui->WeatherView->setModel(WeatherLocTblMdl);
    ui->WeatherView->setColumnWidth(0,200);
    ui->WeatherView->setColumnWidth(1,80);
    ui->WeatherView->setColumnWidth(2,80);
    ui->WeatherView->setColumnWidth(4,80);
    ui->WeatherView->setColumnWidth(5,80);
    ui->WeatherView->setColumnWidth(6,80);
    ui->WeatherView->hideColumn(3); //wmo
    ui->WeatherView->hideColumn(7); //timezone

    prepareWeatherFilters();

}

void LocalDataManager::prepareWeatherFilters()
{
    QSqlQuery *regions = new QSqlQuery("Select distinct Region from location",WeatherDB);
    int regionidx = regions->record().indexOf("Region");
    ui->WeatherRegionFilter->addItem(QIcon(),"Region");
    while (regions->next())
    {
        ui->WeatherRegionFilter->addItem(QIcon(),regions->value(regionidx).toString());
    }

    QSqlQuery *countries = new QSqlQuery("Select distinct Country from location",WeatherDB);
    int countryidx = countries->record().indexOf("Country");
    ui->WeatherCountryFilter->addItem(QIcon(),"Country");
    while (countries->next())
    {
        ui->WeatherCountryFilter->addItem(QIcon(),countries->value(countryidx).toString());
    }

    resetWeatherFilters();
}

void LocalDataManager::resetWeatherFilters()
{
    resetCountryFilter();
    resetRegionFilter();
    resetCityFilter();

    WeatherLocTblMdl->setFilter("");
    reloadModelViews();
}

void LocalDataManager::resetCityFilter()
{
    ui->WeatherCityFilter->setText(tr("City"));
    ui->WeatherCityFilter->selectAll();
}

void LocalDataManager::resetRegionFilter()
{
    ui->WeatherRegionFilter->setCurrentIndex(0);
}

void LocalDataManager::resetCountryFilter()
{
    ui->WeatherCountryFilter->setCurrentIndex(0);
}

QSqlRecord LocalDataManager::selectWeatherLocation()
{
    int rc;

    ui->PageList->hide();
    ui->WeatherLocationPage->activateWindow();
    rc = exec();

    if (rc ==1)
    {
        QSqlRecord retrec(WeatherLocTblMdl->record(ui->WeatherView->currentIndex().row()));
        qDebug() << retrec;
        return retrec;
    }
    else
        return QSqlRecord();
}

void LocalDataManager::on_WeatherView_doubleClicked(const QModelIndex &index)
{
    if (ui->PageList->isVisible() == false)
        accept();
}

void LocalDataManager::on_WeatherCityFilter_textEdited(const QString &arg1)
{
    resetCountryFilter();
    resetRegionFilter();
    WeatherLocTblMdl->setFilter("City LIKE '%" + ui->WeatherCityFilter->text() + "%'");
    reloadModelViews();
}

void LocalDataManager::on_WeatherRegionFilter_currentIndexChanged(const QString &arg1)
{
    resetCountryFilter();
    resetCityFilter();
    WeatherLocTblMdl->setFilter("Region LIKE '%" + ui->WeatherRegionFilter->currentText() + "%'");
    reloadModelViews();
}

void LocalDataManager::on_WeatherCountryFilter_currentIndexChanged(const QString &arg1)
{
    resetRegionFilter();
    resetCityFilter();
    WeatherLocTblMdl->setFilter("Country LIKE '%" + ui->WeatherCountryFilter->currentText() + "%'");
    reloadModelViews();
}

void LocalDataManager::reloadModelViews()
{
    WeatherLocTblMdl->select();
//    addPointsToMap();
}

void LocalDataManager::on_WeatherFilterClear_clicked()
{
    resetWeatherFilters();
}

void LocalDataManager::createNewLibraryDB()
{
    bool success = false;
    LibraryDB.open();
    QSqlQuery query(LibraryDB);

    success = query.exec("create table surface_types(label text, rvalue real)");
    success = query.exec("create table component_types(label text, load real)");
    success = query.exec("create table schedules(label text, factor real)");

    // then populate them somehow

    if (!success)
    {
        QMessageBox::critical(0, tr("New Project Failure"),query.lastError().text(), QMessageBox::Cancel);
        return;
    }
}


//void LocalDataManager::addPointsToMap()
//{
//        QDomDocument       kmlDocument;
////        QDomImplementation impl;
//        QDomProcessingInstruction instr = kmlDocument.createProcessingInstruction("xml","version=\"1.0\" encoding=\"UTF-8\"");
//        kmlDocument.appendChild(instr);
//        QDomElement kmlRoot = kmlDocument.createElementNS( "http://earth.google.com/kml/2.1","kml");
//        kmlDocument.appendChild(kmlRoot);

////        double lng = 0;
////        double lat = 0;

//        for (int i = 0; i < WeatherLocTblMdl->rowCount(); i++)
//        {

//            QDomText locationtext = kmlDocument.createTextNode(WeatherLocTblMdl->data(WeatherLocTblMdl->index(i,0)).toString());
//            QDomText coordinatestext = kmlDocument.createTextNode(WeatherLocTblMdl->data(WeatherLocTblMdl->index(i,5)).toString()
//                                        + "," + WeatherLocTblMdl->data(WeatherLocTblMdl->index(i,4)).toString() + ",0");
//            QDomText descriptiontext = kmlDocument.createTextNode(tr("Weather Data"));
//            QDomElement name = kmlDocument.createElement("name");
//            name.appendChild(locationtext);
//            QDomElement description = kmlDocument.createElement("description");
//            description.appendChild(descriptiontext);
//            QDomElement coordinates = kmlDocument.createElement("coordinates");
//            coordinates.appendChild(coordinatestext);
//            QDomElement point = kmlDocument.createElement("point");
//            point.appendChild(coordinates);
//            QDomElement placemark = kmlDocument.createElement("placemark");
//            placemark.appendChild(name);
//            placemark.appendChild(description);
//            placemark.appendChild(point);
//            kmlRoot.appendChild(placemark);

//        }
//        qDebug() << kmlDocument.toString();

//        if (WeatherLocTblMdl->rowCount()>0)
//        {
////            d->marbleWidget->setHome(lng, lat);
////            d->marbleWidget->centerOn(lng, lat);
////            ui->MarbleWidget->addGeoDataFile("kmlpoints.xml");
////            ui->MarbleWidget->addGeoDataString(kmlDocument.toString(),"placemark");
//        }

//}
