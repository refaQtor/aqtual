-- TABLE CREATION

-- model_relation_types
CREATE TABLE model_relation_types
(id uuid PRIMARY KEY,
type text NOT NULL);

-- model_relations
CREATE TABLE model_relations
(this uuid NOT NULL,
type uuid NOT NULL,
that uuid NOT NULL,
FOREIGN KEY(type)
REFERENCES model_relation_types(type) );

-- model_relations
CREATE TABLE model_object_types
(id uuid PRIMARY KEY,
type text NOT NULL);

-- model_objects
CREATE TABLE model_objects
(id uuid PRIMARY KEY,
type uuid NOT NULL,
label text DEFAULT '-',
FOREIGN KEY(type) REFERENCES model_object_types(type) );

-- model_field_types
CREATE TABLE model_field_types
(id uuid PRIMARY KEY,
type text NOT NULL);

-- model_field_units
CREATE TABLE model_field_units
(id uuid PRIMARY KEY,
units text NOT NULL,
conversion real NOT NULL);

-- model_fields
CREATE TABLE model_fields
(id uuid PRIMARY KEY,
type uuid NOT NULL,
units uuid NOT NULL,
label text DEFAULT '-',
FOREIGN KEY(type) REFERENCES model_field_types(type)
FOREIGN KEY(units) REFERENCES model_field_units(units) );

-- model_points
CREATE TABLE model_points
(id uuid NOT NULL,
point_order integer,
x_val real,
y_val real,
z_val real);





-- DB INTERFACES

-- interface_object_model_points
CREATE VIEW interface_object_model_points
AS SELECT
model_objects.label AS 'label',
model_points.point_order AS 'order',
model_points.x_val AS 'x',
model_points.y_val AS 'y',
model_points.z_val AS 'z'
FROM model_points
JOIN model_objects
ON model_objects.id = model_points.id;

-- interface_object_list
CREATE VIEW interface_object_list
AS SELECT
model_objects.label AS 'label',
model_object_types.type AS 'type'
FROM model_objects
JOIN model_object_types
ON model_object_types.id = model_objects.type;

-- INTERFACE TRIGGERS



-- DEFAULT INSERTS



--INSERT INTO model_object_types VALUES( '%1', 'box')").arg(QUuid::createUuid().toString().remove("{").remove("}"));
--    success = query.exec(box);

