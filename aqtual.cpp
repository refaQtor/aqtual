/**********************************************************************
        Copyright 2014 Shannon Mackey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**********************************************************************/

#include "aqtual.h"
#include "ui_aqtual.h"

#include <QFileDialog>
#include <QAction>
#include <QMessageBox>
#include <QDateTime>
#include <QUuid>
#include <QtDebug>

aQtual::aQtual(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::aQtual),
    DataMan(new LocalDataManager(this)),
    m_sequence(0)
{
    QCoreApplication::setOrganizationName("praQtice");
    QCoreApplication::setOrganizationDomain("praQtice.org");
    QCoreApplication::setApplicationName("aQtual");

    ui->setupUi(this);
    readSettings();
    prepareActions();
    prepareWindowMenu();
    prepareGlobalDBs();


    ui->SituFrame->setDataManager(DataMan);

    connect(ui->menu_Window,SIGNAL(aboutToShow()),this,SLOT(prepareWindowMenu()));
}

aQtual::~aQtual()
{
    delete ui;
}


void aQtual::prepareActions()
{
    prepareWindowActions();

    ProjectGroup = new QActionGroup(this);
    ProjectGroup->setExclusive(false);
    ProjectGroup->addAction(ui->action_Close);
    ProjectGroup->addAction(ui->actionClose_All);
    ProjectGroup->addAction(ui->action_Duplicate);
    ProjectGroup->addAction(ui->action_Project_Search);
    ProjectGroup->addAction(ui->action_All_Projects_Search);
    ProjectGroup->setEnabled(false);

    DirtyGroup = new QActionGroup(this);
    DirtyGroup->setExclusive(false);
    DirtyGroup->addAction(ui->action_Save);
    DirtyGroup->addAction(ui->actionSave_All);
    DirtyGroup->setEnabled(false);

}

void aQtual::prepareWindowActions()
{   connect(ui->action_Tile,SIGNAL(triggered()),ui->mdiArea,SLOT(tileSubWindows()));
    WindowActions.append(ui->action_Tile);

    connect(ui->action_Cascade,SIGNAL(triggered()),ui->mdiArea,SLOT(cascadeSubWindows()));
    WindowActions.append(ui->action_Cascade);

    connect(ui->action_Next,SIGNAL(triggered()),ui->mdiArea,SLOT(activateNextSubWindow()));
    WindowActions.append(ui->action_Next);

    connect(ui->action_Previous,SIGNAL(triggered()),ui->mdiArea,SLOT(activatePreviousSubWindow()));
    WindowActions.append(ui->action_Previous);
}

void aQtual::prepareGlobalDBs()
{
    LibraryDB = QSqlDatabase::addDatabase("QSQLITE", "library");
    LibraryDB.setDatabaseName("library.slt");

    QFile llibfile(LibraryDB.databaseName());
    if (llibfile.exists())
    {
        qDebug() << llibfile.fileName();
        if (!LibraryDB.open())
        {
            QMessageBox::critical(0,"File Access Error","Can't open Library DB file: " + llibfile.fileName(), QMessageBox::Cancel);
        }
    }


}

ModelWindow *aQtual::createMdiChild()
{
    QFile filename(QFileDialog::getSaveFileName(this, tr("New aQtual File"), QDir::toNativeSeparators(QDir::homePath()), tr("aQtual (*.qsf)")));
    if (!filename.fileName().isEmpty()) {
        if (filename.fileName().endsWith("qsf",Qt::CaseInsensitive))
            return createMdiChild(filename.fileName());
        else
            return createMdiChild(filename.fileName() + ".qsf");
    }
    return createMdiChild(QUuid::createUuid().toString().remove("{"));//handles case where file dialog is canceled
}

ModelWindow *aQtual::createMdiChild(QString fname)
{
    QString filename = fname;
    if (!filename.isEmpty()) {
        QMdiSubWindow *existing = findMdiChild(filename);
        if (existing) {
            ui->mdiArea->setActiveSubWindow(existing);
        } else {
            int seq = ++m_sequence;
            ModelWindow *child = new ModelWindow(filename, seq, sequenceColor(seq));
            ui->mdiArea->addSubWindow(child);
            ProjectGroup->setEnabled(true);
            return child;
        }
    }
}

void aQtual::setDirty(bool star)
{
    DirtyGroup->setEnabled(star);
}

void aQtual::on_action_New_triggered()
{
    ModelWindow *child = createMdiChild();
    child->showMaximized();

    connect(child,SIGNAL(dirty(bool)),this,SLOT(setDirty(bool)));

    //    connect(child,SIGNAL(added(int,QString)),ui->ResultsDoc,SLOT(addSeries(int,QString)));
    //    connect(child,SIGNAL(removed(int)),ui->ResultsDoc,SLOT(removeSeries(int)));
    //    connect(child,SIGNAL(seriesDataChanged(int,QList<double>*)),ui->ResultsDoc,SLOT(updateSeries(int,QList<double>*)));

    ProjectGroup->setEnabled(true);
}

void aQtual::closeEvent(QCloseEvent *event)
{
    saveSettings();
    ui->mdiArea->closeAllSubWindows();

    QMainWindow::closeEvent(event);
}

void aQtual::saveSettings()
{
    QSettings settings(QSettings::IniFormat,QSettings::UserScope,"praQtice","aQtual");
    settings.setValue("geometry", saveGeometry());
    settings.setValue("windowState", saveState());

}

void aQtual::readSettings()
{
    QSettings settings(QSettings::IniFormat,QSettings::UserScope,"praQtice","aQtual");
    restoreGeometry(settings.value("geometry").toByteArray());
    restoreState(settings.value("windowState").toByteArray());

    // user configurable via dialog

}

void aQtual::on_action_About_aQtual_triggered()
{
    AboutaQtual* aqb = new AboutaQtual();
    aqb->setWindowTitle("About aQtual");
    aqb->exec();
    delete aqb;
}

void aQtual::on_actionAbout_Qt_triggered()
{
    QApplication::aboutQt ();
}

ModelWindow *aQtual::activeMdiChild()
{
    if (QMdiSubWindow *activeSubWindow = ui->mdiArea->activeSubWindow())
        return qobject_cast<ModelWindow *>(activeSubWindow->widget());
    return 0;
}

QMdiSubWindow *aQtual::findMdiChild(const QString &fileName)
{
    QString canonicalFilePath = QFileInfo(fileName).canonicalFilePath();

    foreach (QMdiSubWindow *window, ui->mdiArea->subWindowList()) {
        ModelWindow *mdiChild = qobject_cast<ModelWindow *>(window->widget());
        if (mdiChild->filename() == canonicalFilePath)
            return window;
    }
    return 0;
}

void aQtual::setActiveSubWindow(QWidget *window)
{
    if (!window)
        return;
    ui->mdiArea->setActiveSubWindow(qobject_cast<QMdiSubWindow *>(window));
}

void aQtual::on_action_Open_triggered()
{
    QString openFilename = QFileDialog::getOpenFileName(this, tr("Open Existing aQtual File"), QDir::toNativeSeparators(QDir::homePath()), tr("aQtual (*.qsf)"));
    if (openFilename != "")
        openExistingFile(openFilename);
}

void aQtual::openExistingFile(const QString &existingname)
{
    ModelWindow *child = createMdiChild(existingname);
    child->showMaximized();
    connect(child,SIGNAL(dirty(bool)),this,SLOT(setDirty(bool)));
    ProjectGroup->setEnabled(true);
}

void aQtual::on_action_Save_triggered()
{
    if (activeMdiChild() && activeMdiChild()->save())
        ui->statusBar->showMessage(tr("Saved:  %1").arg(activeMdiChild()->filename()), 2000);
}

void aQtual::on_action_Duplicate_triggered()
{
    if (activeMdiChild() && activeMdiChild()->save())
    {
        QString activeFilename = activeMdiChild()->filename();
        QString dupeFilename = QFileDialog::getOpenFileName(this, tr("Duplicate aQtual File"), QDir::toNativeSeparators(QDir::homePath()), tr("aQtual (*.qsf)"));
        QFile::copy(activeFilename,dupeFilename);
        openExistingFile(dupeFilename);
    }
}

void aQtual::on_actionSave_All_triggered()
{
    foreach (QMdiSubWindow *window, ui->mdiArea->subWindowList()) {
        ModelWindow *mdiChild = qobject_cast<ModelWindow *>(window->widget());
        mdiChild->save();
    }
}

void aQtual::on_actionClose_All_triggered()
{
    foreach (QMdiSubWindow *window, ui->mdiArea->subWindowList()) {
        ModelWindow *mdiChild = qobject_cast<ModelWindow *>(window->widget());
        mdiChild->close();
    }
    ProjectGroup->setEnabled(false);
}

void aQtual::on_action_Close_triggered()
{
    if (activeMdiChild() && activeMdiChild()->close())
    {
        ui->statusBar->showMessage(tr("Closed:  %1").arg(activeMdiChild()->filename()),2000);
        ui->mdiArea->closeActiveSubWindow();

    }
    if (!activeMdiChild()) ProjectGroup->setEnabled(false);
}

QColor aQtual::sequenceColor(const int seqno)
{
    QColor primary;
    int hue(seqno*57%360);
    int sat(255);
    int val(255);
    primary.setHsv(hue,sat,val);
    return primary;
}

void aQtual::prepareWindowMenu()
{
    ui->menu_Window->clear();
    ui->menu_Window->addActions(WindowActions);
    ui->menu_Window->addSeparator();

    int counter = 0;
    QString menuname;
    QPixmap coloricon(10,10);
    foreach (QMdiSubWindow *window, ui->mdiArea->subWindowList()) {
        counter++;
        ModelWindow *mdiChild = qobject_cast<ModelWindow *>(window->widget());
        coloricon.fill(mdiChild->color());
        menuname = mdiChild->filenameOnly();
        ui->menu_Window->addAction(coloricon,menuname,mdiChild,SLOT(setFocus()),QKeySequence(tr("Alt+%1").arg(counter)));
    }
}

void aQtual::on_action_Results_toggled(bool show)
{
    if(show)
        ui->ResultsDoc->show();
    else
        ui->ResultsDoc->hide();
}

void aQtual::on_action_Situation_toggled(bool show)
{
    if(show)
        ui->SituationDoc->show();
    else
        ui->SituationDoc->hide();
}


void aQtual::on_action_Quit_triggered()
{
    close();
}

void aQtual::on_action_User_Settings_triggered()
{
    UserSettings *setdlg = new UserSettings();
    setdlg->exec();
    delete setdlg;
}

void aQtual::on_action_Data_Manager_triggered()
{
    DataMan->show();
}
