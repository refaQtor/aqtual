/**********************************************************************
        Copyright 2014 Shannon Mackey

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
**********************************************************************/

#include "modelwindow.h"
#include "ui_modelwindow.h"

#include <QtDebug>
#include <QFile>
#include <QMessageBox>
#include <QFileDialog>
#include <QCloseEvent>
#include <QTableView>
#include <QList>

#include "model.h"

ModelWindow::ModelWindow(QWidget *parent)
{
    Q_UNUSED(parent)
}

ModelWindow::ModelWindow(QString fname, int seqno, const QColor &color, QWidget *parent) :
    QFrame(parent),
    ui(new Ui::ModelWindow),
    model(new Model(this,fname)),
    SceneGraph(new QGraphicsScene(this)),
    m_seqnumber(seqno)
{
    ui->setupUi(this);

    setWindowTitle(model->filenameOnly() + "[*]");
    connectUItoModel();
    setColor(color);

//    connect(bgs, SIGNAL(itemInserted(Space*)),
//            this, SLOT(itemInserted(Space*)));

    setAttribute(Qt::WA_DeleteOnClose);

    emit added(m_seqnumber,m_color);
}



ModelWindow::~ModelWindow()
{
    delete ui;
    emit removed(m_seqnumber);
}


void ModelWindow::setDirty(bool star)
{
    setWindowModified(star);
    emit dirty(star);
}

bool ModelWindow::save()
{
    model->saveModel();
}


bool ModelWindow::saveAs()
{
//TODO: do this right
    return close();
}


void ModelWindow::closeFile()
{
    model->closeModel();
}

void ModelWindow::closeEvent(QCloseEvent *event)
{
    if (checkSave()) {
        event->accept();
    } else {
        event->ignore();
    }
}

void ModelWindow::modelModified(QList<QRectF> items)
{
    setWindowModified(true);

}

bool ModelWindow::checkSave()
{
    if (isWindowModified()) {
        QMessageBox::StandardButton ret;
        ret = QMessageBox::warning(this, tr("Confirm..."),
                                   tr("'%1' has been modified.\n\n"
                                      "Do you want to save your changes?")
                                   .arg(filenameOnly()),
                                   QMessageBox::Save | QMessageBox::Discard
                                   | QMessageBox::Cancel);
        if (ret == QMessageBox::Save)
        {
            model->closeModel();
            return save();
        }
        else if (ret == QMessageBox::Cancel)
            return false;
    }
    return true;
}

void ModelWindow::setColor(QColor color)
{
    m_color = color;

    QString stylecolor(tr("hsv(%1,%2,%3)").arg(m_color.hue()).arg(m_color.saturation()).arg(m_color.value()));
    QString stylestring("QFrame { border: 2px solid "+ stylecolor +"}");

    setStyleSheet(stylestring);
}

void ModelWindow::connectUItoModel()
{
    ui->graphicsView->setScene(SceneGraph);
    ui->graphicsView->show();

    for (int var = 0; var < model->TableModels->count(); ++var) {
        QTableView *view = new QTableView;
        QSqlTableModel *tbl = model->TableModels->at(var);
        view->setModel(tbl);
        ui->TableSelector->insertItem(0,model->TableModels->at(var)->tableName());
        ui->TableStack->insertWidget(0,view);
        view->show();
        connect(ui->TableSelector,SIGNAL(currentIndexChanged(int)),ui->TableStack,SLOT(setCurrentIndex(int)));
    }

}
